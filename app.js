var express = require('express');
var app = express();

app.set('view engine', 'ejs');

app.get('/', function(req,res){
    res.send("<html><body>Portal de Notícias</body></html>");
});


app.get('/formulario_inclusao_noticia', function(req, res){
    res.render("admin/form_add_noticia");
});

app.get('/tecnologia', function(req,res){
    res.render('secao/tecnologia');
});

app.get('/noticias', function(req,res){                                  
    res.render('secao/noticias');
});

app.get('/moda', function(req,res){
    res.render('secao/moda');
});

app.listen(3000, function(){
    console.log("Servidor rodando com Express");           
});       